import { useContext, useState } from 'react'
import { GlobalContext } from '../context/GlobalState'
import { v4 as uuid } from 'uuid'

const AddTransaction = () => {
  const [text, setText] = useState('')
  const [amount, setAmount] = useState(0)

  const { addTransaction } = useContext(GlobalContext)

  /**
   *
   * @param {Event} e
   */
  const handleSubmit = e => {
    e.preventDefault()
    if (text.trim() === '' || amount === 0) {
      alert('Please add a text and amount')
      return
    }

    const newTransaction = {
      id: uuid(),
      text,
      amount: +amount, // parseFloat(amount)
    }
    
    addTransaction(newTransaction)
  }

  return (
    <>
      <h3>Add new transaction</h3>
      <form onSubmit={handleSubmit}>
        <div className="form-control">
          <label htmlFor="text">Text</label>
          <input type="text" placeholder="Enter text..." value={text} onChange={e => setText(e.target.value)} />
        </div>
        <div className="form-control">
          <label htmlFor="amount">
            Amount <br />
            (negative - expense, positive - income)
          </label>
          <input type="number" placeholder="Enter amount..." value={amount} onChange={e => setAmount(e.target.value)} />
        </div>
        <button className="btn">Add transaction</button>
      </form>
    </>
  )
}

export default AddTransaction
